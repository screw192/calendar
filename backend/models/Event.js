const mongoose = require('mongoose');

const EventSchema = new mongoose.Schema({
  datetime: {
    type: Date,
    required: true,
  },
  eventName: {
    type: String,
    required: true,
  },
  duration: {
    type: Number,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  }
});

const Event = mongoose.model('Event', EventSchema);
module.exports = Event;
const express = require("express");
const User = require("../models/User");
const config = require("../config");
const axios = require("axios");
const auth = require("../middleware/auth");
const {nanoid} = require("nanoid");
const { OAuth2Client } = require("google-auth-library")

const googleClient = new OAuth2Client(config.google.clientId);
const router = express.Router();

router.post("/", async (req, res) => {
  try {
    const user = new User({
      email: req.body.email,
      password: req.body.password,
      displayName: req.body.displayName,
    });

    user.generateToken();
    await user.save();
    return res.send(user);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.post("/sessions", async (req, res) => {
  const user = await User.findOne({email: req.body.email}).populate("sharedFriends", "email");

  if (!user) {
    return res.status(401).send({message: "Credentials are wrong"});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(401).send({message: "Credentials are wrong"});
  }

  user.generateToken();
  await user.save();

  return res.send({message: "Email and password correct!", user});
});

router.delete("/sessions", async (req, res) => {
  const token = req.get("Authorization");
  const success = {message: "Success"};

  if (!token) return res.send(success);

  const user = await User.findOne({token});

  if (!user) return res.send(success);

  user.generateToken();

  await user.save();

  return res.send(success);
});

router.post("/facebookLogin", async (req, res) => {
  const inputToken = req.body.accessToken;

  const accessToken = config.facebook.appId + "|" + config.facebook.appSecret;

  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

  try {
    const response = await axios.get(debugTokenUrl);

    if (response.data.data.error) {
      return res.status(401).send({global: "Facebook token incorrect"});
    }

    if (response.data.data["user_id"] !== req.body.id) {
      return res.status(401).send({global: "User ID incorrect"});
    }

    let user = await User.findOne({email: req.body.email}).populate("sharedFriends", "email");

    if (!user) {
      user = await User.findOne({facebookId: req.body.id}).populate("sharedFriends", "email");
    }

    if (!user) {
      user = new User({
        email: req.body.email || nanoid(),
        password: nanoid(),
        facebookId: req.body.id,
        displayName: req.body.name,
      });
    }

    user.generateToken();
    await user.save();

    res.send({message: "Success", user});
  } catch (e) {
    return res.status(401).send({global: "Facebook token incorrect"})
  }
});

router.post("/googleLogin", async (req, res) => {
  try {
    const ticket = await googleClient.verifyIdToken({
      idToken: req.body.tokenId,
      audience: process.env.CLIENT_ID
    });

    const {name, email, sub: ticketUserId} = ticket.getPayload();

    if (req.body.googleId !== ticketUserId) {
      return res.status(401).send({global: "User ID incorrect"});
    }

    let user = await User.findOne({email}).populate("sharedFriends", "email");

    if (!user) {
      user = new User({
        email,
        password: nanoid(),
        displayName: name,
      });
    }

    user.generateToken();
    await user.save();

    res.send({message: "Success", user});
  } catch (e) {
    console.error(e);
    return res.status(500).send({global: "Server error. Please try again."});
  }
});

router.patch("/", auth, async (req, res) => {
  try {
    const sharedUser = await User.findOne({email: req.body.email});

    if (!sharedUser) {
      return res.status(404).send({message: "User not found"});
    }

    let user;
    switch (req.body.type) {
      case "share":
        user = await User.findOneAndUpdate(
            {_id: req.user._id},
            {$addToSet: {sharedFriends: sharedUser._id}},
            {new: true}
        ).populate("sharedFriends", "email");
        break;
      case "remove":
        user = await User.findOneAndUpdate(
            {_id: req.user._id},
            {$pull: {sharedFriends: {$in: sharedUser._id}}},
            {new: true}
        ).populate("sharedFriends", "email");
        break;
      default:
        user = await User.findOne({_id: req.user._id}).populate("sharedFriends", "email");
        break;
    }

    res.send(user);
  } catch (error) {
    return res.status(400).send(error);
  }
});

module.exports = router;
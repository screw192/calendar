const express = require("express");
const auth = require("../middleware/auth");
const Event = require("../models/Event");
const User = require("../models/User");

const router = express.Router();

router.get("/", auth, async (req, res) => {
  try {
    const sharedUsersData = await User.find({sharedFriends: req.user._id}).select("_id");
    const sharedUsersId = Object.values(sharedUsersData);
    const searchIds = [req.user._id, ...sharedUsersId];

    const now = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());

    const events = await Event.find({user: searchIds, datetime: {$gte: now}}).sort("datetime");

    res.send(events);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", auth, async (req, res) => {
  try {
    const eventData = {
      datetime: req.body.datetime,
      eventName: req.body.eventName,
      duration: req.body.duration,
      user: req.user._id,
    }

    const event = new Event(eventData);
    await event.save();

    res.send(event);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete("/:id", auth, async (req, res) => {
  try {
    const targetEvent = await Event.findOne({_id: req.params.id});

    if (toString(targetEvent.user) !== toString(req.user._id)) {
      return res.status(403).send({message: "You are allowed to remove only your own events!"});
    }

    await Event.deleteOne({_id: req.params.id});

    res.send({message: "Event successfully deleted"});
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
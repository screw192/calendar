const mongoose = require("mongoose");
const config = require("./config");
const User = require("./models/User");
const Event = require("./models/Event");
const {nanoid} = require("nanoid");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const user359 = await User.create({
    email: "user359@test",
    password: "user",
    token: nanoid(),
    displayName: "User 359"
  });

  const [user69, user666] = await User.create({
    email: "user69@test",
    password: "user",
    token: nanoid(),
    displayName: "User 69",
    sharedFriends: [user359]
  }, {
    email: "user666@test",
    password: "user",
    token: nanoid(),
    displayName: "User 666"
  });

  await Event.create({
    datetime: "2021-05-21T08:30:00.000Z",
    eventName: "Call Johnny Silverhand",
    duration: 0.5,
    user: user69,
  }, {
    datetime: "2021-05-25T10:15:00.000Z",
    eventName: "Find Adam Smasher",
    duration: 1,
    user: user666,
  }, {
    datetime: "2021-06-02T04:45:00.000Z",
    eventName: "Visit Vic to upgrade mods",
    duration: 1.5,
    user: user359,
  }, {
    datetime: "2021-05-28T02:30:00.000Z",
    eventName: "Contact with Rogue at bar",
    duration: 1.5,
    user: user69,
  }, {
    datetime: "2021-05-27T10:00:00.000Z",
    eventName: "Find Cyberpsycho at Kabuki district",
    duration: 1,
    user: user359,
  });

  await mongoose.connection.close();
};

run().catch(console.error);
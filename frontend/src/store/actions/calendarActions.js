import calendarSlice from "../slices/calendarSlice";

export const {
  fetchCalendarRequest,
  fetchCalendarSuccess,
  fetchCalendarFailure,
  addEventRequest,
  addEventSuccess,
  addEventFailure,
  removeEventRequest,
  removeEventFailure,
} = calendarSlice.actions;
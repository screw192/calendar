import {combineReducers} from "redux";
import usersSlice from "./slices/usersSlice";
import calendarSlice from "./slices/calendarSlice";


const rootReducer = combineReducers({
  users: usersSlice.reducer,
  calendar: calendarSlice.reducer,
});

export default rootReducer;
import {put, takeEvery} from 'redux-saga/effects';
import axiosApi from "../../axiosApi";
import {
  addEventFailure,
  addEventRequest,
  fetchCalendarFailure,
  fetchCalendarRequest,
  fetchCalendarSuccess,
  removeEventFailure,
  removeEventRequest
} from "../actions/calendarActions";


export function* fetchCalendar() {
  try {
    const response = yield axiosApi.get("/events");
    yield put(fetchCalendarSuccess(response.data));
  } catch (e) {
    yield put(fetchCalendarFailure(e.response.data));
  }
}

export function* addEvent({payload: eventData}) {
  try {
    yield axiosApi.post("/events", eventData);
    yield put(fetchCalendarRequest());
  } catch (e) {
    yield put(addEventFailure(e.response.data));
  }
}

export function* removeEvent({payload: eventId}) {
  try {
    yield axiosApi.delete(`/events/${eventId}`);
    yield put(fetchCalendarRequest());
  } catch (e) {
    yield put(removeEventFailure(e.response.data));
  }
}

const calendarSagas = [
  takeEvery(fetchCalendarRequest, fetchCalendar),
  takeEvery(addEventRequest, addEvent),
  takeEvery(removeEventRequest, removeEvent),
];

export default calendarSagas;
import {put, takeEvery} from 'redux-saga/effects';
import axiosApi from "../../axiosApi";
import {
  facebookLoginRequest, googleLoginRequest,
  loginFailure, loginRequest,
  loginSuccess, logoutRequest,
  logoutSuccess,
  registerFailure,
  registerRequest,
  registerSuccess, shareCalendarFailure, shareCalendarRequest, shareCalendarSuccess
} from "../actions/usersActions";
import {historyPush} from "../actions/historyActions";

export function* registerUser({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users', userData);
    yield put(registerSuccess(response.data));
    yield put(historyPush('/'));
  } catch (error) {
    yield put(registerFailure(error.response.data));
  }
}

export function* loginUser({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users/sessions', userData);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush('/'));
  } catch (error) {
    yield put(loginFailure(error.response.data));
  }
}

export function* facebookLogin({payload: data}) {
  try {
    const response = yield axiosApi.post('/users/facebookLogin', data);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush('/'));
  } catch (error) {
    yield put(loginFailure(error.response.data));
  }
}

export function* googleLogin({payload: {tokenId, googleId}}) {
  try {
    const body = {tokenId, googleId};
    const response = yield axiosApi.post('/users/googleLogin', body);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush('/'));
  } catch (error) {
    yield put(loginFailure(error.response.data));
  }
}

export function* logout() {
  yield axiosApi.delete('/users/sessions');
  yield put(logoutSuccess());
  yield put(historyPush('/'));
}

export function* shareCalendar({payload: data}) {
  try {
    console.log("email:", data.email, "type: ", data.type);
    const response = yield axiosApi.patch("/users", {email: data.email, type: data.type});
    console.log(response.data);
    yield put(shareCalendarSuccess(response.data));
  } catch (error) {
    yield put(shareCalendarFailure(error.response.data));
  }
}

const usersSagas = [
  takeEvery(registerRequest, registerUser),
  takeEvery(loginRequest, loginUser),
  takeEvery(facebookLoginRequest, facebookLogin),
  takeEvery(googleLoginRequest, googleLogin),
  takeEvery(logoutRequest, logout),
  takeEvery(shareCalendarRequest, shareCalendar),
];

export default usersSagas;
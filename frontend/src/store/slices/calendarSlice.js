import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  calendar: [],
  calendarLoading: false,
  calendarError: null,
};

const name = "calendar";

const calendarSlice = createSlice({
  name,
  initialState,
  reducers: {
    fetchCalendarRequest: state => {
      state.calendarLoading = true;
    },
    fetchCalendarSuccess: (state, {payload: calendar}) => {
      state.calendarLoading = false;
      state.calendar = calendar;
    },
    fetchCalendarFailure: (state, {payload: error}) => {
      state.calendarLoading = false;
      state.calendarError = error;
    },
    addEventRequest: state => {
      state.calendarLoading = true;
    },
    addEventSuccess: () => {},
    addEventFailure: (state, {payload: error}) => {
      state.calendarLoading = false;
      state.calendarError = error;
    },
    removeEventRequest: state => {
      state.calendarLoading = true;
    },
    removeEventSuccess: () => {},
    removeEventFailure: (state, {payload: error}) => {
      state.calendarLoading = false;
      state.calendarError = error;
    },
  }
});

export default calendarSlice;
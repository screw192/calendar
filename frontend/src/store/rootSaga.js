import {all} from 'redux-saga/effects';

import historySagas from "./sagas/historySagas";
import history from "../history";
import usersSagas from "./sagas/usersSagas";
import calendarSagas from "./sagas/calendarSagas";


export default function* rootSaga() {
  yield all([
    ...historySagas(history),
    ...usersSagas,
    ...calendarSagas,
  ]);
};
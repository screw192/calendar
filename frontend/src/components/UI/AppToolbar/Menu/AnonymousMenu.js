import React from 'react';
import {Link} from "react-router-dom";
import {Button} from "@material-ui/core";

const AnonymousMenu = () => {
  return (
    <>
      <Button component={Link} to="/login" color="primary" disableElevation>Login</Button>
      <Button component={Link} to="/register" color="primary" disableElevation>Register</Button>
    </>
  );
};

export default AnonymousMenu;
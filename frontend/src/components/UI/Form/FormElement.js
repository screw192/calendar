import React from 'react';
import {Grid, MenuItem, TextField} from "@material-ui/core";

const FormElement = ({error, select, options, ...props}) => {
  let inputChildren = null;

  if (select) {
    inputChildren = options.map(option => (
      <MenuItem key={option._id} value={option._id}>
        {option.title}
      </MenuItem>
    ));
  }

  return (
    <Grid item xs>
      <TextField
        select={select}
        error={Boolean(error)}
        helperText={error}
        {...props}
      >
        {inputChildren}
      </TextField>
    </Grid>
  );
};

export default FormElement;
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Container, Grid, IconButton, Typography} from "@material-ui/core";

import {addEventRequest, fetchCalendarRequest} from "../../store/actions/calendarActions";
import CalendarItem from "./CalendarItem";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import FormElement from "../../components/UI/Form/FormElement";


const initialState = {
  datetime: "",
  eventName: "",
  duration: "",
};

const Calendar = () => {
  const dispatch = useDispatch();
  const userData = useSelector(state => state.users.user);
  const eventsData = useSelector(state => state.calendar.calendar);
  const [eventData, setEventData] = useState({
    datetime: "",
    eventName: "",
    duration: "",
  });

  useEffect(() => {
    if (userData) {
      dispatch(fetchCalendarRequest());
    }
  }, [userData]);

  const inputChangeHandler = e => {
    let {name, value} = e.target;

    if (name === "duration") {
      if (value < 0) {
        value = value * -1;
      }
    }

    setEventData(prev => ({...prev, [name]: value}));
  };

  const addEventHandler = () => {
    dispatch(addEventRequest(eventData));
    setEventData({...initialState});
  };

  const eventList = eventsData.map(calendarEvent => {
    return (
        <CalendarItem
            key={calendarEvent._id}
            id={calendarEvent._id}
            datetime={calendarEvent.datetime}
            eventName={calendarEvent.eventName}
            duration={calendarEvent.duration}
            owner={calendarEvent.user === userData?._id}
        />
    );
  });

  return (
      <Container maxWidth="lg">
        <Grid container direction="column">
          {userData ? (
              <>
                <Grid container alignItems="center">
                  <Grid item xs={3}>
                    <FormElement
                        size="small"
                        type="datetime-local"
                        name="datetime"
                        value={eventData.datetime}
                        onChange={inputChangeHandler}
                    />
                  </Grid>
                  <Grid item xs>
                    <FormElement
                        label="Event name"
                        size="small"
                        type="text"
                        name="eventName"
                        value={eventData.eventName}
                        onChange={inputChangeHandler}
                    />
                  </Grid>
                  <Grid item xs={2}>
                    <FormElement
                        label="Duration (minutes)"
                        size="small"
                        type="number"
                        name="duration"
                        value={eventData.duration}
                        onChange={inputChangeHandler}
                    />
                  </Grid>
                  <Grid item>
                    <IconButton
                        onClick={addEventHandler}
                    >
                      <AddCircleIcon color="primary"/>
                    </IconButton>
                  </Grid>
                </Grid>
                {eventList}
              </>
          ) : (
              <Typography variant="h5">Login to see your planned events</Typography>
          )}
        </Grid>
      </Container>
  );
};

export default Calendar;
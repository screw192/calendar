import React from 'react';
import {useDispatch} from "react-redux";
import {Grid, IconButton, makeStyles, Paper, Typography} from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';
import moment from "moment";

import {removeEventRequest} from "../../store/actions/calendarActions";


const useStyles = makeStyles({
  eventItem: {
    marginBottom: "20px",
    padding: "10px 65px 10px 10px",
    position: "relative",
  },
  eventName: {
    margin: "0 10px",
    padding: "0 10px",
  },
  button: {
    position: "absolute",
    top: "50%",
    right: "10px",
    transform: "translateY(-50%)",
  }
});

const CalendarItem = ({id, datetime, eventName, duration, owner}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const dateBeauty = moment(datetime).format("DD MMM YY");
  const timeBeauty = moment(datetime).format("HH:mm");
  const durationBeauty = duration > 60 ?
      `${Math.floor(duration / 60)} h ${(duration % 60)} min` :
      `${duration} min`;

  const removeEventHandler = () => {
    dispatch(removeEventRequest(id));
  };

  return (
      <Grid item container component={Paper} className={classes.eventItem} alignContent="center">
        <Grid item xs={2} component={Paper} variant="outlined">
          <Typography variant="body1" align="center">{dateBeauty} at {timeBeauty}</Typography>
        </Grid>
        <Grid item xs  component={Paper} variant="outlined" className={classes.eventName}>
          <Typography variant="body1">{eventName}</Typography>
        </Grid>
        <Grid item xs={2} component={Paper} variant="outlined">
          <Typography variant="body1" align="center">{durationBeauty}</Typography>
        </Grid>
        {owner && (
            <IconButton
                className={classes.button}
                onClick={removeEventHandler}
            >
              <DeleteIcon color="secondary"/>
            </IconButton>
        )}
      </Grid>
  );
};

export default CalendarItem;
import React from 'react';
import {useDispatch} from "react-redux";
import {Grid, IconButton, Typography} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

import {shareCalendarRequest} from "../../store/actions/usersActions";


const SharesListItem = ({email}) => {
  const dispatch = useDispatch();

  const removeShareHandler = () => {
    dispatch(shareCalendarRequest({email, type: "remove"}));
  };

  return (
      <Grid item container alignItems="center">
        <Grid item xs={4}>
          <Typography variant="body1">
            {email}
          </Typography>
        </Grid>
        <Grid item>
          <IconButton
              onClick={removeShareHandler}
          >
            <DeleteIcon color="secondary"/>
          </IconButton>
        </Grid>
      </Grid>
  );
};

export default SharesListItem;
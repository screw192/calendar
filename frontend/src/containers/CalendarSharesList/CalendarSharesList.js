import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Container, Grid, IconButton, Typography} from "@material-ui/core";
import AddCircleIcon from '@material-ui/icons/AddCircle';

import SharesListItem from "./SharesListItem";
import FormElement from "../../components/UI/Form/FormElement";
import {shareCalendarRequest} from "../../store/actions/usersActions";


const CalendarSharesList = () => {
  const dispatch = useDispatch();
  const sharedFriends = useSelector(state => state.users.user?.sharedFriends);
  const [email, setEmail] = useState("");

  let sharesList = null;
  if (sharedFriends) {
    sharesList = sharedFriends.map(item => {
      return (
          <SharesListItem
              id={item._id}
              email={item.email}
          />
      );
    });
  }

  const inputChange = event => {
    setEmail(event.target.value);
  };

  const addEmailHandler = () => {
    dispatch(shareCalendarRequest({email, type: "share"}));
    setEmail("");
  };

  return (
      <Container maxWidth="md">
        <Typography variant="h6" paragraph>
          Shared users list
        </Typography>
        <Grid container alignItems="center">
          <Grid item xs={4}>
            <FormElement
                size="small"
                value={email}
                onChange={inputChange}
            />
          </Grid>
          <Grid item>
            <IconButton
                onClick={addEmailHandler}
            >
              <AddCircleIcon color="primary"/>
            </IconButton>
          </Grid>
        </Grid>
        <Grid container direction="column">
          {sharesList}
        </Grid>
      </Container>
  );
};

export default CalendarSharesList;
import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import {useSelector} from "react-redux";
import {Helmet} from "react-helmet";

import Layout from "./components/UI/Layout/Layout";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import Calendar from "./containers/Calendar/Calendar";
import CalendarSharesList from "./containers/CalendarSharesList/CalendarSharesList";


const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
      <Route {...props} /> :
      <Redirect to={redirectTo}/>;
};

const App = () => {
  const user = useSelector(state => state.users.user);

  return (
      <Layout>
        <Helmet
            titleTemplate="Joojle calendar - %s"
            defaultTitle="Joojle calendar"
        />
        <Switch>
          <Route path="/" exact component={Calendar}/>
          <Route path="/login" component={Login}/>
          <Route path="/register" component={Register}/>
          <ProtectedRoute
              path="/shares"
              component={CalendarSharesList}
              isAllowed={user}
              redirectTo="/login"
          />
        </Switch>
      </Layout>
  );
};

export default App;